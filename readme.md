#Social Media KPIs Sheet

[Google Apps Script](https://developers.google.com/apps-script/) files, with accompanying Google Sheet, designed to automate the fetching, storing and display of key performance indicators from Facebook, Twitter and Google Analytics. Original concept by [Avinash Kaushik](http://www.kaushik.net/avinash/best-social-media-metrics-conversation-amplification-applause-economic-value/).

---
##Requirements
1. [Google Docs account](https://docs.google.com/)
2. Facebook page and [Facebook API Key](https://developers.facebook.com/)
3. Twitter account and [Twitter API key](https://dev.twitter.com/)
4. [Google Analytics account](http://www.google.com/analytics/)

---
##Setup
###General
1. [Import](https://support.google.com/docs/answer/40608) the `social_media_kpis_sheet.xlsx` spreadsheet into your Google Drive
1. Open the `social_media_kpis_sheet.xlsx` and launch the Script Editor from the Tools menu.
1. Replicate the files contained within `gs_files` within the Script Editor interface
1. Create a folder named `archive` on your Google Drive in the same directory as `social_media_kpis_sheet.xlsx` for backups


###twitter.gs
1. Edit the`consumerKey` value to match your Consumer Key
1. Edit the `consumerSecret` value to match your Consumer Secret key
1. Edit the `screen_name` value to match your username
1. Press the run button in the Google Apps Script editor, to manually authorise access to your Twitter account for the first time


###facebook.gs
1. Edit the `appIdFacebook` value to match your App ID
1. Edit the `appSecretFacebook` value to match your App Secret key


###analytics.gs
1. Ensure that the Google account your Google Sheet is setup on has access to the Google Analytics account that you want to collect data from. **The script is currently only able to pull data from the first Google Analytics profile it encounters**. Consequently if you have more than one Google Analytics account, you may want to configure your Google Sheet on a separate Google account and [provide access](http://www.techwyse.com/blog/website-analytics/giving-access-to-google-anaytics/) to only the specific Google Analytics account you need to pull data from
1. Check that the values assigned to `filterArgsTwitter` and `filterArgsFacebook` match how social media traffic is identified in your Google Analytics setup
1. Configure the `metrics` attribute to match the goal(s) you want to track and update the `sort` attribute to order output by completions of your new goal

NB: The `analytics.gs` script is based upon the code at: [https://developers.google.com/analytics/solutions/articles/reporting-apps-script](https://developers.google.com/analytics/solutions/articles/reporting-apps-script). A useful reference of possible metrics can be found at: [https://developers.google.com/analytics/devguides/reporting/core/dimsmets#cats=goal_conversions](https://developers.google.com/analytics/devguides/reporting/core/dimsmets#cats=goal_conversions)

###generalutils.gs
1. Edit the `sheet_id` value to the id of your Google Sheet ([as found in the Google Sheet URL](https://productforums.google.com/forum/#!topic/docs/RzpxH1KhLCA))
1. Edit the value of `emailAddress` with an email address that you would like to receive error notifications to
1. Set up [time-driven triggers](https://developers.google.com/apps-script/guides/triggers/installable#time-driven_triggers) for each script. See example configuration below:

![Example time-driven trigger configuration](https://bytebucket.org/worldanimalprotectionuk/social-media-kpis/raw/456952abae017553c30842ab5a9cf627df49f9d9/imgs/current_project_triggers.jpg "Example time-driven trigger configuration")

---
##Possible Improvements
1. Improve error handling, especially for scenarios where rate limits are hit or APIs are unavailable
1. Improve `analytics.gs` code to avoid failure when more than one Google Analytics account is accessible
1. Automate more of the setup procedure and improve setup documentation
1. Modify code to reflect existence of and take advantage of JavaScript closures

---
##License
The MIT License (MIT)
Copyright (c) 2014 World Animal Protection
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.