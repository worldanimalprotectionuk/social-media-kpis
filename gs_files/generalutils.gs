//HELPER FUNCTIONS
//--------------------------------------------------------------------------------------------------  

//sheet id
var sheetId = "<ENTER GOOGLE SHEET ID>"; //EDIT THIS

//FUNCTION
//function to create js date object from iso8601 date input from facebook
function dateFromISO8601(isostr) {
  var parts = isostr.match(/\d+/g);
  return new Date(parts[0], parts[1] - 1, parts[2], parts[3], parts[4], parts[5]);
}

//FUNCTION
//function to create js date object from date input from twitter
function parseTwitterDate(text) {
  return new Date(Date.parse(text.replace(/( +)/, ' UTC$1')));
}

//FUNCTION
//create yesterday's date as js date object
var dateYest = new Date();
dateYest.setDate(dateYest.getDate() - 1); // -1 day / 24hrs ago

//FUNCTION
//send email to notify user of errors
function sendEmail(errorMessage) {
   var emailAddress = "<ENTER EMAIL ADDRESS>"; //EDIT THIS
   var subject = "Notification from KPI API Requests Script";
   var message = errorMessage;
   MailApp.sendEmail(emailAddress, subject, message); // send email
   Logger.log("Error email sent");
}

//FUNCTION
//print returned values to spreadsheet
function printValToSheet(cellRef, cellInput, activeRow, sheet) {
  //output share value
  var cellFetch = cellRef + activeRow; // build cell reference
  var cellQuery = sheet.setActiveCell(cellFetch).getValue(); //get value of cell

  //check cell value is empty
  if (cellQuery == "") {
    var cellDate = sheet.setActiveCell(cellFetch).setValue(cellInput); //input value into cell
  } else {
    errorMessage = "Error: cell " + cellFetch + " in facebookdata already contains a value";
    Logger.log(errorMessage);
    sendEmail(errorMessage); //email error message
  }
}

//FUNCTION
//find yesterdays date in spreadsheet and log row number (currently only used by facebook and twitter - not by google analytics)
function rowFinder(sheet) {
//get date values from sheet
var range = sheet.getRange(2, 1, 367); //get specified range (looking at date column)
var data = range.getValues(); //get values from specified range and place in 2d array

//iterate over returned array
for(n=0; n < data.length; ++n){ 
  var inputDate = new Date(data[n][0]); //create date object from value in array
  //Logger.log(inputDate);
    
    //check if date in sheet match yesterdays date created in "setup" section of script
    if(inputDate.getDate() == dateYest.getDate() && inputDate.getMonth() == dateYest.getMonth() && inputDate.getFullYear() == dateYest.getFullYear()) {
      
      //NB: brittle way of retrieving cell reference, but currently only way possible. any changes to sheet will break script
      n+=2; //increment value of row by 2 to account for "Date" header in sheet and discrepancy between sheet (which counts from 1) and script (which counts from 0)
      var activeRow = n; //store returned value in activeRow var
      Logger.log("Found row: " + n);
    }
}

  //double check that activeRow var is set before continuing. Likely to have run out of dates in spreadsheet if it isn't
  if (activeRow == undefined) {
    errorMessage = "Error: row number is not set. It is likely that you need to add more dates to your sheet.";
    sendEmail(errorMessage); //email error message
    throw errorMessage; //kill script execution
  }

  return activeRow;
}


//FUNCTION
//make external api call (only used by facebook at the moment)
function makeapiCall(endPoint) {
var url = encodeURI(endPoint);
try {
    var json = UrlFetchApp.fetch(url);
}
catch(errorMessage) {
    Logger.log(errorMessage);
    sendEmail(errorMessage);
}

json = JSON.parse(json); //parse returned value
return json;
}