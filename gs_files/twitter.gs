//SETUP
//--------------------------------------------------------------------------------------------------  
//retweet variables
var retweetCount = 0;
var retweetTotal = 0;

//favourite variables
var favouritesCount = 0;
var favouritesTotal = 0;

//replies
var repliesTotal = 0;

//tweet id array
var tweetTrack = [];

//column references
var postsCol = "B"; //Posts
var idsCol = "F"; //IDs
var retweetsCol = "D"; //Retweets
var favsCol = "E"; //Favourites
var repliesCol = "C"; //Replies



//AUTHORISATION
//--------------------------------------------------------------------------------------------------  
function queryTwitter() {

var consumerKey = '<ENTER API KEY>'; //API key
var consumerSecret = '<ENTER API SECRET KEY>'; //API secret
var userScreenName = '<ENTER USERNAME>'; //username

var oauthConfig = UrlFetchApp.addOAuthService('twitter');
oauthConfig.setAccessTokenUrl('https://api.twitter.com/oauth/access_token');
oauthConfig.setRequestTokenUrl('https://api.twitter.com/oauth/request_token');
oauthConfig.setAuthorizationUrl('https://api.twitter.com/oauth/authorize');
oauthConfig.setConsumerKey(consumerKey);
oauthConfig.setConsumerSecret(consumerSecret);

// "twitter" value must match the argument to "addOAuthService" above.
var options = {
    'oAuthServiceName' : 'twitter',
    'oAuthUseToken' : 'always'
    //'muteHttpExceptions' : true
};


//CALL TO TWITTER API
//--------------------------------------------------------------------------------------------------  
function makeapiCallTwitter(endPoint) {
var url = encodeURI(endPoint);
try {
    var response = UrlFetchApp.fetch(url, options);
}
    catch(errorMessage) {
      Logger.log(errorMessage);
      sendEmail(errorMessage);
}

var json = JSON.parse(response.getContentText());
return json;
}



//FETCH DATA
//-------------------------------------------------------------------------------------------------- 
var initialTwitterCall = 'https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=' + userScreenName;
var json = makeapiCallTwitter(initialTwitterCall);


//GET CORRECT SPREADSHEET AND SHEET
//-------------------------------------------------------------------------------------------------- 
//get spreadsheet by id and sheet by name
var sheet = SpreadsheetApp.openById(sheetId).getSheetByName("twitterdata");

//find yesterday's date in sheet and assign to variable
var activeRow = rowFinder(sheet); 




//CHECK NO.1
//CHECK RETURNED TWITTER DATA AGAINST YESTERDAYS DATE FOR NUMBER OF POSTS
//--------------------------------------------------------------------------------------------------  

//loop through data returned from facebook
for (var i = 0; i < json.length; i++) {  
     
//if created time exists in json
  if (json[i].created_at !== undefined) {
          
  //Logger.log(json[i].created_at);
  var postDate = parseTwitterDate(json[i].created_at);
  //Logger.log(postDate);
  
  //check if created date of post from twitter is same as yesterdays date created in "setup" section of script
    if (postDate.getDate() == dateYest.getDate() && postDate.getMonth() == dateYest.getMonth() && postDate.getFullYear() == dateYest.getFullYear()) {
    
      //Logger.log("Posts - found match: " + postDate);
      postTotal++; //increment post counter when post is found
    
      //put id of tweet in array for future reference
        if (json[i].id_str !== undefined) {
          tweetTrack.push(json[i].id_str);
          //Logger.log(tweetTrack);
        }
        
        } else {
          //Logger.log("Posts - no match: " + postDate);
    } 
  }
} //close for loop

Logger.log("Total posts: " + postTotal); //logs posts total
printValToSheet(postsCol, postTotal, activeRow, sheet); //call function to print POSTS value to sheet
printValToSheet(idsCol, tweetTrack, activeRow, sheet); //call function to print IDS value to sheet




//CHECK NO.2
//CHECK RETURNED TWITTER DATA AGAINST YESTERDAYS DATE FOR RETWEETS
//--------------------------------------------------------------------------------------------------  

//loop through data returned from facebook
for (var i = 0; i < json.length; i++) {  
     
  //if created time exists in json
  if (json[i].created_at !== undefined) {
    
    var postDate = parseTwitterDate(json[i].created_at);
    //Logger.log(postDate);
    
    //check if created date of post from facebook is same as yesterdays date created in "setup" section of script
    if (postDate.getDate() == dateYest.getDate() && postDate.getMonth() == dateYest.getMonth() && postDate.getFullYear() == dateYest.getFullYear()) {
            
      //Logger.log("Retweets - found match: " + postDate);
      //Logger.log(json[i].retweet_count);
      
      //add up number of shares - CORE SHARE FUNCTIONALITY
      if (json[i].retweet_count !== undefined) {
        retweetCount = json[i].retweet_count; //get value
        retweetCount = Number(retweetCount); //ensure returned value is a number
        retweetTotal += retweetCount; //add returned value to variable
      }
            
      } else {
        //Logger.log("Retweets - no match: " + postDate);
    }     
  }
} //close for loop

Logger.log("Total retweets: " + retweetTotal); //logs shares total
printValToSheet(retweetsCol, retweetTotal, activeRow, sheet); //call function to print REWTEETS value to sheet




//CHECK NO.3
//MAKE 2nd QUERY BASED ON TWITTER IDs FOR FAVOURITES
//--------------------------------------------------------------------------------------------------  

for(var i = 0; i < tweetTrack.length; i++) {
        
  var tweetId = tweetTrack[i]; //get first tweet id from array

  //REQUEST SPECIFIC TWEET
  var jsonTweet = makeapiCallTwitter('https://api.twitter.com/1.1/statuses/show.json?id=' + tweetId);
 
//GET VALUE FOR FAVOURITES         
  if (jsonTweet.favorite_count !== undefined) {
    favouritesCount = Number(jsonTweet.favorite_count); //ensure returned value is a number
    Logger.log("Favs for " + tweetId + " "  + favouritesCount);
    favouritesTotal += favouritesCount; //add returned value to variable            
  }
  
} //close for loop           


Logger.log("Total favourites " + favouritesTotal);
printValToSheet(favsCol, favouritesTotal, activeRow, sheet); //call function to print FAVOURITES value to sheet




//CHECK NO.4
//MAKE 2nd QUERY BASED ON TWITTER IDs FOR REPLIES
//--------------------------------------------------------------------------------------------------  
//GET VALUE FOR REPLIES
for(var i = 0; i < tweetTrack.length; i++) {
            
  //Logger.log(tweetTrack[i]);   
  var tweetId = tweetTrack[i];
  
  //CALL TWITTER API
  var jsonTweet = makeapiCallTwitter('https://api.twitter.com/1.1/statuses/mentions_timeline.json?count=200&include_rts=0');
 
  //GET VALUE FOR REPLIES
  for (var i = 0; i < jsonTweet.length; i++) {
    if (jsonTweet[i].in_reply_to_status_id_str !== undefined) { 
                  
      var replyValue = jsonTweet[i].in_reply_to_status_id_str;
      Logger.log(replyValue);
                  
        if(replyValue == tweetId) {
          Logger.log("Reply found");
          repliesTotal ++; //increment counter
        }      
    }
  }
  
  
  //PAGING TWITTER RESULTS
  //log id of last value retrieved
  var lastTweet = Number(jsonTweet.length) -1;
  Logger.log(lastTweet);
  //Logger.log("LAST TWEET " + lastTweet);
  if (jsonTweet[lastTweet].id !== undefined) {
    var maxId = jsonTweet[lastTweet].id;
    Logger.log("MAX " + maxId);
  }
  
  //call twitter api again
  var jsonReplies = makeapiCallTwitter('https://api.twitter.com/1.1/statuses/mentions_timeline.json?count=200&include_rts=0&max_id=' + maxId);
  
  Logger.log(jsonReplies);
  
  
  //GET VALUE FOR REPLIES PART 2
  for (var i = 0; i < jsonReplies.length; i++) {
    if (jsonReplies[i].in_reply_to_status_id_str !== undefined) { 
                  
      var replyValue = jsonReplies[i].in_reply_to_status_id_str;
      Logger.log(replyValue);
                  
        if(replyValue == tweetId) {
          Logger.log("Reply found");
          repliesTotal ++; //increment counter
        }      
    }
  }
  
  //NB: currently fetching and checking against approx last 400 tweets
 
  
} //close for loop


Logger.log("Total replies " + repliesTotal);
printValToSheet(repliesCol, repliesTotal, activeRow, sheet); //call function to print FAVOURITES value to sheet




//OUTPUT time of execution to "Updated column"
//-------------------------------------------------------------------------------------------------- 
var cellFetch = "G" + activeRow;
var cellDate = sheet.setActiveCell(cellFetch).setValue(timeStamp);

} //close function