function backUp() {
  //Create a duplicate of the spreadsheet named by time and date as backup
  var SSID = sheetId;
  var CopyDate = new Date(); //create date and specify format
  var folder = DocsList.getFolder('archive'); //folder to save to by name
  var backup = DocsList.getFileById(SSID).makeCopy(SpreadsheetApp.openById(SSID).getName() + "_" + CopyDate);
  backup.addToFolder(folder); //move the file to the folder
  backup.removeFromFolder(DocsList.getRootFolder()); //remove file from appearing in root. user will have to look in archive "folder" / tag
}