//SETUP
//--------------------------------------------------------------------------------------------------  
//post variables 
var postTotal = 0;

//share variables
var shareCount = 0;
var shareTotal = 0;

//comment variables
var commentCount = 0;
var commentTotal = 0;

//like variables
var likeCount = 0;
var likeTotal = 0;

//track ids of posts
var facebookTrack = [];

//misc variables
var errorMessage;
  
//time variables
var createdTime; //var for facebook date and time
var timeStamp = new Date(); //current date and time for logging time of execution to "Updated" column
//var currentDate = new Date("August 1, 2014"); //artifical date for testing purposes

//column references
var postsCol = "B"; //Posts
var idsCol = "F"; //IDs
var sharesCol = "D"; //Shares
var likesCol = "E"; //Likes
var commentsCol = "C"; //Comments
var updatedCol = "G"; //Updated

//keys
var appIdFacebook = '<ENTER APP ID>';
var appSecretFacebook = '<ENTER APP SECRET KEY>';
var facebookUserName = '<ENTER FACEBOOK PAGE NAME>';

//FETCH JSON POSTS DATA FROM FACEBOOK - 1st request
//--------------------------------------------------------------------------------------------------


var json = makeapiCall('https://graph.facebook.com/' + facebookUserName + '/posts?key=value&access_token=' + appIdFacebook + '|' + appSecretFacebook);




//FETCH JSON DATA SUMMARY FROM FACEBOOK - 2nd request
//--------------------------------------------------------------------------------------------------
var jsonSummary = makeapiCall('https://graph.facebook.com/' + facebookUserName + '/feed?fields=comments.limit(1).summary(true),likes.limit(1).summary(true)&key=value&access_token=' + appIdFacebook + '|' + appSecretFacebook);




//--------------------------------------------------------------------------------------------------
//QUERY FACEBOOK DATA
//--------------------------------------------------------------------------------------------------
function queryFacebookData() {  
  
//get spreadsheet by id and sheet by name
var sheet = SpreadsheetApp.openById(sheetId).getSheetByName("facebookdata");

//find yesterday's date in sheet and assign to variable
var activeRow = rowFinder(sheet);




//CHECK No.1
//CHECK RETURNED FACEBOOK DATA AGAINST YESTERDAYS DATE FOR NUMBER OF POSTS, IDs OF POSTS AND NUMBER OF SHARES
//USES MAIN JSON REQUEST
//--------------------------------------------------------------------------------------------------  

//loop through data returned from facebook
  for (var i = 0; i < json.data.length; i++) {  
     
    //if created time exists in json
    if (json.data[i].created_time !== undefined) {
          var postDate = dateFromISO8601(json.data[i].created_time); //call function (found in helper functions) to convert date returned from facebook to usable format
 
          //check if created date of post from facebook is same as yesterdays date created in "setup" section of script
          if (postDate.getDate() == dateYest.getDate() && postDate.getMonth() == dateYest.getMonth() && postDate.getFullYear() == dateYest.getFullYear()) {
            
            
            //POST NUMBER SECTION
            Logger.log("Posts - found match: " + postDate);
            postTotal++; //increment post counter when post is found
          
         
            //POST ID SECTION
            //put id of post in array for future reference
            if (json.data[i].id !== undefined) {
              var postInput = json.data[i].id;
              var postResult = postInput.replace(/.*[_]/,''); //regular expression which only returns post id after underscore
              facebookTrack.push(postResult);
            }           
          
          
            //SHARE SECTION
            if (json.data[i].shares !== undefined && json.data[i].shares.count !== undefined) {
              shareCount = json.data[i].shares.count; //get value
              shareCount = Number(shareCount); //ensure returned value is a number
              shareTotal += shareCount; //add returned value to variable
            } //close share conditional
        
        
     
          } else {
            Logger.log("Posts / Shares - no match: " + postDate);
          } 
    }
  } //close for loop

  Logger.log("Total posts: " + postTotal); //logs posts total
  Logger.log("Total shares: " + shareTotal); //logs shares total

printValToSheet(postsCol, postTotal, activeRow, sheet); //call function to print POSTS value to sheet
printValToSheet(idsCol, facebookTrack, activeRow, sheet); //call function to print IDs value to sheet
printValToSheet(sharesCol, shareTotal, activeRow, sheet); //call function to print SHARES value to sheet




//CHECK No.2
//CHECK RETURNED FACEBOOK DATA AGAINST YESTERDAYS DATE FOR LIKES & COMMENTS
//USES JSON SUMMARY API REQUEST
//--------------------------------------------------------------------------------------------------  

//loop through data returned from facebook
  for (var i = 0; i < jsonSummary.data.length; i++) {  
     
    //if created time exists in json
    if (jsonSummary.data[i].created_time !== undefined) {
          var postDate = dateFromISO8601(jsonSummary.data[i].created_time); //call function (found in helper functions) to convert date returned from facebook to usable format
 
          //check if created date of post from facebook is same as yesterdays date created in "setup" section of script
          if (postDate.getDate() == dateYest.getDate() && postDate.getMonth() == dateYest.getMonth() && postDate.getFullYear() == dateYest.getFullYear()) {
            
            
            //add up number of likes - CORE LIKE FUNCTIONALITY
            if (jsonSummary.data[i].likes !== undefined && jsonSummary.data[i].likes.summary !== undefined && jsonSummary.data[i].likes.summary.total_count !== undefined) {
              Logger.log("Likes - found match: " + postDate);
              likeCount = Number(jsonSummary.data[i].likes.summary.total_count); //get value and ensure it's a number
              likeTotal += likeCount;
              Logger.log(likeTotal);
            } //close comment conditional
            
            
            //add up number of comments - CORE COMMENT FUNCTIONALITY
            if (jsonSummary.data[i].comments !== undefined && jsonSummary.data[i].comments.summary !== undefined && jsonSummary.data[i].comments.summary.total_count !== undefined) {
              Logger.log("Comments - found match: " + postDate);
              commentCount = Number(jsonSummary.data[i].comments.summary.total_count); //get value and ensure it's a number
              commentTotal += commentCount;
              Logger.log(commentTotal);
            } //close comment conditional
            
            
          } else {
            //Logger.log("Likes/com - no match: " + postDate);
          } 
    }
  } //close for loop

  Logger.log("Total likes: " + likeTotal); //logs shares total
  Logger.log("Total comments: " + commentTotal); //logs shares total


printValToSheet(likesCol, likeTotal, activeRow, sheet); //call function to print LIKES value to sheet
printValToSheet(commentsCol, commentTotal, activeRow, sheet); //call function to print COMMENTS value to sheet




//OUTPUT time of execution to "Updated column"
//-------------------------------------------------------------------------------------------------- 
var cellFetch = updatedCol + activeRow;
var cellDate = sheet.setActiveCell(cellFetch).setValue(timeStamp);

} //close function