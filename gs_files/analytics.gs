// helper date function
function getLastNdays(nDaysAgo) {
  var today = new Date();
  var before = new Date();
  before.setDate(today.getDate() - nDaysAgo);
  return Utilities.formatDate(before, 'GMT', 'yyyy-MM-dd');
}


//CORE FUNCION - control script flow
function queryAnalytics() {
  try {
  
    //filter arguments
    var filterArgsTwitter = 'ga:source==twitter.com';
    var filterArgsFacebook = 'ga:source==facebook.com,ga:source==m.facebook.com,ga:source==l.facebook.com,ga:source==apps.facebook.com,ga:source==lm.facebook.com,ga:source==partners.facebook.com';
        
    //function calls
    var firstProfile = getFirstProfile();

    var resultsTwitter = getReportDataForProfile(firstProfile, filterArgsTwitter);
    var resultsFacebook = getReportDataForProfile(firstProfile, filterArgsFacebook); //call 2 for facebook
    
    outputToSpreadsheet(resultsTwitter, 0);
    outputToSpreadsheet(resultsFacebook, 1);
    
  } catch(error) {
    Logger.log(error);
  }
}



//FUNCTION - RETRIEVE FIRST VIEW (PROFILE) FUNCTION
//get the id of the first account retrieved
function getFirstProfile() {
  var accounts = Analytics.Management.Accounts.list();
  if (accounts.getItems()) {
    var firstAccountId = accounts.getItems()[0].getId();

    //if web properties exist for id
    var webProperties = Analytics.Management.Webproperties.list(firstAccountId);
    if (webProperties.getItems()) {

      //get profiles
      var firstWebPropertyId = webProperties.getItems()[0].getId();
      var profiles = Analytics.Management.Profiles.list(firstAccountId, firstWebPropertyId);

      if (profiles.getItems()) {
        var firstProfile = profiles.getItems()[0];
        return firstProfile; //return first profile

      } else {
        sendEmail('No views (profiles) found.');
        throw new Error('No views (profiles) found.');
      }
    } else {
      sendEmail('No webproperties found.');
      throw new Error('No webproperties found.');
    }
  } else {
    sendEmail('No accounts found.');
    throw new Error('No accounts found.');
  }
}








//FUNCTION - use core reporting api to retrieve data
function getReportDataForProfile(firstProfile, filterArgs) {

  var profileId = firstProfile.getId();
  var tableId = 'ga:' + profileId;
  var startDate = getLastNdays(1);    // from yesterday
  var endDate = getLastNdays(1);      // to today

  var optArgs = {
    'metrics': 'ga:goal5Completions,ga:goal5Value',
    'dimensions': 'ga:source',     // Comma separated list of dimensions.
    'sort': '-ga:goal5Completions',       // Sort by goal compeletion
    //'segment': 'dynamic::ga:isMobile==Yes',  // Process only mobile traffic.
    'filters': filterArgs, //filter by argument for either twitter or facebook
    'start-index': '1',
    'max-results': '250'                     // Display the first 250 results.
  };
  
// Make a request to the API.
  var results = Analytics.Data.Ga.get(
      tableId,                    // Table id (format ga:xxxxxx).
      startDate,                  // Start-date (format yyyy-MM-dd).
      endDate,                    // End-date (format yyyy-MM-dd).
      'ga:sessions,ga:pageviews', // Comma seperated list of metrics.
      optArgs);

  if (results.getRows()) {
    return results;

  } else {
    //no results found, return 0 for everything
    results = {
      rows: 0
    };   
    return results;
    //throw new Error('No views (profiles) found');
    
  }
}





//FUNCTION - output to sheet
function outputToSpreadsheet(results, sourceCheck) {
  var sheet = SpreadsheetApp.openById(sheetId).getSheetByName("gadata");

  //find yesterday's date in sheet and assign to variable
  var activeRow = rowFinder(sheet);


   //calculate total
   var totalGoals = 0;
   var totalValue = 0;
   for (var i = 0; results.rows[i]; ++i) {
     totalGoals += Number(results.rows[i][1]); //iterate over rows in column 1 - in current request column 1 is where goal number is stored
     totalValue += Number(results.rows[i][2]); //goal value
  }
  
  Logger.log(totalGoals);
  Logger.log(totalValue);
  
  //check if we're printing values for facebook or twitter
  if (sourceCheck === 0) {
    //print to sheet
    printValToSheet('b', totalGoals, activeRow, sheet); //number of conversions refactored
    printValToSheet('c', totalValue, activeRow, sheet); //value of conversions refactored
  } else {
    printValToSheet('d', totalGoals, activeRow, sheet); //number of conversions refactored
    printValToSheet('e', totalValue, activeRow, sheet); //value of conversions refactored
  }


//OUTPUT time of execution to "Updated column"
//-------------------------------------------------------------------------------------------------- 
var cellFetch = "F" + activeRow;
var cellDate = sheet.setActiveCell(cellFetch).setValue(timeStamp);
  
}